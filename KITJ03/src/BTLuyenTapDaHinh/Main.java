package BTLuyenTapDaHinh;

import BaiTapBuoi7.Bai01.SachGiaoKhoa;
import BaiTapBuoi7.Bai01.SachThamKhao;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<KhachHangVN> khachHangVNs = new ArrayList<>();
        ArrayList<KhanhHangNuocNgoai> khanhHangNuocNgoais = new ArrayList<>();
    }
    public static void menu(){
        System.out.println("1. Nhập danh sách : ");
        System.out.println("2. In danh sách");
        System.out.println("3. Tính tổng số lượng");
        System.out.println("4. Trung bình thành tiền khách hàng nước ngoài");
        System.out.println("5. In hóa đơn trong tháng 9/2019");
    }
    public static void nhapDanhSach(ArrayList<KhachHangVN> khachHangVNs, ArrayList<KhanhHangNuocNgoai> khanhHangNuocNgoais) throws Exception {
        do {
            System.out.println("1. Khách hàng Việt Nam");
            System.out.println("2. Khách hàng nước ngoài");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            switch (chon){
                case 1:{
                    nhapKHVN(khachHangVNs);
                    break;
                }
                case 2:{
                    nhapKHNN (khanhHangNuocNgoais);
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void nhapKHVN(ArrayList<KhachHangVN> khachHangVNs) throws Exception {
        System.out.println("Nhập khách hàng Việt Nam : ");
        do {
            KhachHangVN khachHangVN = new KhachHangVN();
            khachHangVN.nhap();
            khachHangVNs.add(khachHangVN);
            System.out.println("1. Nhập tiếp \n\t2. Thoát");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            if (chon == 2)
                return;
        }while (true);
    }
    public static void nhapKHNN(ArrayList<KhanhHangNuocNgoai> khanhHangNuocNgoais)throws Exception{
        System.out.println("Nhập khách hàng Việt Nam : ");
        do {
            KhanhHangNuocNgoai khanhHangNuocNgoai = new KhanhHangNuocNgoai();
            khanhHangNuocNgoai.nhap();
            khanhHangNuocNgoais.add(khanhHangNuocNgoai);
            System.out.println("1. Nhập tiếp \n\t2. Thoát");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            if (chon == 2)
                return;
        }while (true);
    }
    public static void  hienThiDanhSach(ArrayList<KhachHangVN> khachHangVNs, ArrayList<KhanhHangNuocNgoai> khanhHangNuocNgoais){
        do {
            System.out.println("1. Khách hàng Việt Nam");
            System.out.println("2. Khách hàng nước ngoài");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            switch (chon){
                case 1:{
                    //khachHangVN(khachHangVNs);
                    break;
                }
                case 2:{
                    //khachHangNN(khanhHangNuocNgoais);
                    break;
                }
                default:return;
            }
        }while (true);
    }
}
