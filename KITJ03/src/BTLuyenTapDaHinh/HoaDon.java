package BTLuyenTapDaHinh;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class HoaDon {
    private String maKhacHang;
    private String hoTen;
    private Date ngayRaHoaDon;
    private int soLuong;
    private int donGia;

    public String getMaKhacHang() {
        return maKhacHang;
    }

    public void setMaKhacHang(String maKhacHang) {
        this.maKhacHang = maKhacHang;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public Date getNgayRaHoaDon() {
        return ngayRaHoaDon;
    }

    public void setNgayRaHoaDon (String ngayRaHoaDon) throws  Exception{
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        this.ngayRaHoaDon = dateFormat.parse(ngayRaHoaDon);
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }
    public int thanhTien(){
        return 0;
    }
    public void nhap() throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mã khách hàng : ");
        String maKH = scanner.nextLine();
        this.setMaKhacHang(maKH);
        System.out.println("Họ tên : ");
        String hoTen = scanner.nextLine();
        this.setHoTen(hoTen);
        System.out.println("Ngày ra hóa đơn : ");
        String ngayRaHoaDon = scanner.nextLine();
        this.setNgayRaHoaDon(ngayRaHoaDon);
        System.out.println("Đơn giá : ");
        int donGia = scanner.nextInt();
        this.setDonGia(donGia);
        System.out.println("Số lượng : ");
        int soLuong = scanner.nextInt();
        this.setSoLuong(soLuong);
    }
    public void xuat(){
        System.out.println("Mã khách hàng : " + this.getMaKhacHang());
        System.out.println("Họ tên : " + this.getHoTen());
        System.out.println("Ngày ra hóa đơn : " + this.getNgayRaHoaDon());
        System.out.println("Đơn giá : " + this.getDonGia());
        System.out.println("Số lượng : " + this.getSoLuong());
    }
}
