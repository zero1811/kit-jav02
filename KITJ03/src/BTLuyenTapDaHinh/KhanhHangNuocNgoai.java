package BTLuyenTapDaHinh;

import java.util.Scanner;

public class KhanhHangNuocNgoai extends HoaDon {
    private String quocTich;

    public String getQuocTich() {
        return quocTich;
    }

    public void setQuocTich(String quocTich) {
        this.quocTich = quocTich;
    }

    @Override
    public int thanhTien() {
        return this.getSoLuong() * this.getDonGia();
    }
    public void nhap() throws Exception {
        super.nhap();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Quốc tịch : ");
        String quocTich = scanner.nextLine();
        this.setQuocTich(quocTich);
    }
    public void xuat(){
        super.xuat();
        System.out.println("Quốc tịch : " + this.getQuocTich());
    }
}
