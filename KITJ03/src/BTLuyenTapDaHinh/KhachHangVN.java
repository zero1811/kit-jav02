package BTLuyenTapDaHinh;

import java.util.Scanner;

public class KhachHangVN extends HoaDon {
    private String doiTuong;
    private int dinhMuc;

    public String getDoiTuong() {
        return doiTuong;
    }

    public void setDoiTuong(String doiTuong) {
        this.doiTuong = doiTuong;
    }

    public int getDinhMuc() {
        return dinhMuc;
    }

    public void setDinhMuc(int dinhMuc) {
        this.dinhMuc = dinhMuc;
    }

    @Override
    public int thanhTien() {
        int thanhTien = 0;
        if (this.getSoLuong() <= this.getDinhMuc())
            thanhTien = this.getSoLuong() * this.getDonGia();
        else
            thanhTien = (int) (this.getSoLuong() * this.getDonGia() * this.getDinhMuc() + (this.getSoLuong() - this.getDinhMuc()) * this.getDonGia() * 2.5);
        return thanhTien;
    }
    public void nhap() throws Exception {
        super.nhap();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Đối tượng");
        String doiTuong = scanner.nextLine();
        this.setDoiTuong(doiTuong);
        System.out.println("Định mức : ");
        int dinhMuc = scanner.nextInt();
        this.setDinhMuc(dinhMuc);
    }
    public void xuat (){
        super.xuat();
        System.out.println("Đối tượng : " + this.getDoiTuong());
        System.out.println("Định mức : " + this.getDinhMuc());
    }
}
