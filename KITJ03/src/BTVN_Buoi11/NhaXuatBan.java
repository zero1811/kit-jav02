package BTVN_Buoi11;

import java.util.Scanner;

public class NhaXuatBan {
    private String maNXB;
    private String tenNXB;
    private String diaChi;

    public String getMaNXB() {
        return maNXB;
    }

    public void setMaNXB(String maNXB) {
        this.maNXB = maNXB;
    }

    public String getTenNXB() {
        return tenNXB;
    }

    public void setTenNXB(String tenNXB) {
        this.tenNXB = tenNXB;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }
    public NhaXuatBan nhapNXB(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mã nhà xuất bản : ");
        this.setMaNXB(scanner.nextLine());
        System.out.println("Tên nhà xuất bản : ");
        this.setTenNXB(scanner.nextLine());
        System.out.println("Địa chỉ : ");
        this.setDiaChi(scanner.nextLine());
        return new NhaXuatBan();
    }
    public void inNXB(){
        System.out.println("Mã nhà xuất bản : " + this.getMaNXB());
        System.out.println("Tên nhà xuất bản : " + this.getTenNXB());
        System.out.println("Địa chỉ : " + this.getDiaChi());
    }
}
