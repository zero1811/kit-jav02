package BTVN_Buoi11;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Bai1 {
    public static void main(String[] args) {
        ArrayList<Integer> numberList = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            numberList.add(random.nextInt(100));
        }
        System.out.println("Danh sách các số vừa tạo : ");
        for (int i = 0; i < numberList.size(); i++) {
            System.out.println("vị trí " + i + " : " + numberList.get(i));
        }
        ArrayList<Integer> primeNumberList = new ArrayList<>();
        for (int i = 0; i < numberList.size(); i++) {
            if (checkPrime(numberList.get(i)))
                primeNumberList.add(numberList.get(i));
        }
        System.out.println("Danh sách số nguyên tố được tạo từ danh sách trên : ");
        for (int i = 0; i < primeNumberList.size(); i++) {
            System.out.println("vị trí " + i + " : " + primeNumberList.get(i));
        }
        numberList.sort(Integer::compareTo);
        System.out.println("Danh sách sau khi đc sắp xếp : ");
        for (int i = 0; i < numberList.size(); i++) {
            System.out.println("vị trí " + i + " : " + numberList.get(i));
        }
    }
    public static boolean checkPrime(int number){
        if (number < 2)
            return false;
        else{
            for (int i = 2; i < (int)Math.sqrt(number); i++) {
                if (number % i == 0)
                    return false;
            }
            return true;
        }
    }
}
