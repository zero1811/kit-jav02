package BTVN_Buoi11;

import java.util.Scanner;

public class Sach {
    private String maSach;
    private String tenSach;
    private String maTacGia;
    private String maNXB;

    public String getMaSach() {
        return maSach;
    }

    public void setMaSach(String maSach) {
        this.maSach = maSach;
    }

    public String getTenSach() {
        return tenSach;
    }

    public void setTenSach(String tenSach) {
        this.tenSach = tenSach;
    }

    public String getMaTacGia() {
        return maTacGia;
    }

    public void setMaTacGia(String maTacGia) {
        this.maTacGia = maTacGia;
    }

    public String getMaNXB() {
        return maNXB;
    }

    public void setMaNXB(String maNXB) {
        this.maNXB = maNXB;
    }
    public Sach nhapSach(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mã sách : ");
        this.setMaSach(scanner.nextLine());
        System.out.println("Tên sách : ");
        this.setTenSach(scanner.nextLine());
        System.out.println("Ma tác giả");
        this.setMaTacGia(scanner.nextLine());
        System.out.println("Ma nhà xuất bản : ");
        this.setMaNXB(scanner.nextLine());
        return new Sach();
    }
    public void inSach(){
        System.out.println("Mã sách : " + this.getMaSach());
        System.out.println("Tên sách : " + this.getTenSach());
        System.out.println("Mã tác giả : " + this.getMaTacGia());
        System.out.println("Mã nhà xuất bản : " + this.getMaNXB());
    }
}
