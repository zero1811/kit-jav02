package BTVN_Buoi11;

import java.util.ArrayList;
import java.util.Scanner;

public class Bai2 {
    public static void main(String[] args) {
        ArrayList<Sach> DSSach = new ArrayList<>();
        ArrayList<TacGia> DSTacGia = new ArrayList<>();
        ArrayList<NhaXuatBan> DSNXB = new ArrayList<>();
    }
    public static void menu(){
        System.out.println("1. Nhập");
        System.out.println("2. Tìm kiếm theo tên tác giả");
        System.out.println("3. Sửa thông tin sách");
        System.out.println("4. In số sách của một tác giải");
    }
    public static void menunhap(){
        System.out.println("1. Nhập sách");
        System.out.println("2. Nhập tác giải");
        System.out.println("3. Nhập nhà xuất bản");
    }
    public static void nhap( ArrayList<Sach> DSSach,ArrayList<TacGia> DSTacGia,ArrayList<NhaXuatBan> DSNXB){
        do {
            menunhap();
            Scanner scanner = new Scanner(System.in);
            switch (scanner.nextInt()){
                case 1:{
                    nhapSach(DSSach);
                    break;
                }
                case 2:{
                    nhapTacGia(DSTacGia);
                    break;
                }
                case 3:{
                    nhapNXB(DSNXB);
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void nhapSach(ArrayList<Sach> DSSach){
        do {
            System.out.println("Nhập thông tin sách : ");
            DSSach.add(new Sach().nhapSach());
            System.out.println("Bạn có muốn nhập tiếp ?");
            if (new Scanner(System.in).nextLine().equalsIgnoreCase("N"))
                return;
        }while (true);
    }
    public static void nhapTacGia(ArrayList<TacGia> DSTacGia){
        do {
            System.out.println("Nhập thông tin sách : ");
            DSTacGia.add(new TacGia().nhapTacGia());
            System.out.println("Bạn có muốn nhập tiếp ?");
            if (new Scanner(System.in).nextLine().equalsIgnoreCase("N"))
                return;
        }while (true);
    }
    public static void nhapNXB(ArrayList<NhaXuatBan> DSNXB){
        do {
            System.out.println("Nhập thông tin sách : ");
            DSNXB.add(new NhaXuatBan().nhapNXB());
            System.out.println("Bạn có muốn nhập tiếp ?");
            if (new Scanner(System.in).nextLine().equalsIgnoreCase("N"))
                return;
        }while (true);
    }
    public static void timSachTheoTacGia(ArrayList<Sach> DSSach,ArrayList<TacGia> DSTacGia){
        System.out.println("Nhập tên tác giả : ");
        Scanner scanner = new Scanner(System.in);
        String tenTacGia = scanner.nextLine();
        for (Sach sach: DSSach) {
            if (getMaTacGia(tenTacGia,DSTacGia).equalsIgnoreCase(sach.getMaTacGia()))
                sach.inSach();
        }

    }
    public static String getMaTacGia(String tenTacGia, ArrayList<TacGia> DSTacGia){
        for (TacGia tacGia: DSTacGia) {
            if (tenTacGia.equalsIgnoreCase(tacGia.getTenTacGia())){
                return tacGia.getMaTacGia();
            }
        }
        System.out.println("Tác giải không tồn tại");
        return null;
    }
    public static void suaThongTin(ArrayList<Sach> DSSach){
        System.out.println("Nhập mã sách cần sửa : ");
        Scanner scanner = new Scanner(System.in);
        String maSach = scanner.nextLine();
        for (Sach sach : DSSach) {
            if(maSach.equalsIgnoreCase(sach.getMaSach()))
                sach.nhapSach();
        }
    }

}
