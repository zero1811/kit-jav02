package BTVN_Buoi11;

import java.util.Scanner;

public class TacGia {
    private String maTacGia;
    private String tenTacGia;

    public String getMaTacGia() {
        return maTacGia;
    }

    public void setMaTacGia(String maTacGia) {
        this.maTacGia = maTacGia;
    }

    public String getTenTacGia() {
        return tenTacGia;
    }

    public void setTenTacGia(String tenTacGia) {
        this.tenTacGia = tenTacGia;
    }
    public TacGia nhapTacGia(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mã tác giả : ");
        this.setMaTacGia(scanner.nextLine());
        System.out.println("Tên tác giải : ");
        this.setTenTacGia(scanner.nextLine());
        return new TacGia();

    }
    public void inTacGia(){
        System.out.println("Mã tác giả : " + this.getMaTacGia());
        System.out.println("Tên tác giải : " + this.getTenTacGia());
    }
}
