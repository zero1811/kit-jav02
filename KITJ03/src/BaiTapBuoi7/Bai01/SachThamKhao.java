package BaiTapBuoi7.Bai01;

import java.util.Scanner;

public class SachThamKhao extends Sach{
    private double thue ;
    private int thanhTien;

    public double getThue() {
        return thue;
    }

    public void setThue(double thue) {
        this.thue = thue;
    }

    public int getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(int thanhTien) {
        this.thanhTien = thanhTien;
    }
    public void nhapThongTin (){
        super.nhapThongTin();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Thuế : ");
        double thue = scanner.nextDouble();
        this.setThue(thue);
        this.thanhTien = (int)(this.getSoLuong() * (this.getDonGia() + this.getThue()));
    }
    public void thongTin (){
        super.thongTin();
        System.out.println("Thuế : " + this.getThue());
        System.out.println("Thành tiền : " + this.getThanhTien());
    }
}