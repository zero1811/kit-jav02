package BaiTapBuoi7.Bai01;

import java.util.Scanner;

public class SachGiaoKhoa extends Sach {
    private String tinhTrang;
    private int thanhTien;

    public String getTinhTrang() {
        return tinhTrang;
    }

    public void setTinhTrang(String tinhTrang) {
        this.tinhTrang = tinhTrang;
    }

    public int getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(int thanhTien) {
        this.thanhTien = thanhTien;
    }
    public void nhapThongTin (){
        super.nhapThongTin();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Tình trạng : ");
        String tinhTrang = scanner.nextLine();
        this.setTinhTrang(tinhTrang);
        if (this.getTinhTrang().equalsIgnoreCase("Mới"))
            this.thanhTien = this.getSoLuong() * this.getDonGia();
        else
            this.thanhTien = (int)(this.getSoLuong() * this.getDonGia() * 0.5);
    }
    public void thongTin (){
        super.thongTin();
        System.out.println("Tình trạng : " + this.getTinhTrang());
        System.out.println("Thành tiền : " + this.getThanhTien());
    }
}
