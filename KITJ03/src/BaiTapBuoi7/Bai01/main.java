package BaiTapBuoi7.Bai01;

import java.util.ArrayList;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        ArrayList<SachGiaoKhoa> danhSachSGK = new ArrayList<SachGiaoKhoa>();
        ArrayList<SachThamKhao> danhSachSTK = new ArrayList<SachThamKhao>();
        Scanner scanner = new Scanner(System.in);
        do {
            menu();
            int chon = scanner.nextInt();
            switch (chon){
                case 1:{
                    nhapDanhSach(danhSachSGK,danhSachSTK);
                    break;
                }
                case 2:{
                    hienThiDanhSach(danhSachSGK,danhSachSTK);
                    break;
                }
                case 3:{
                    tongThanhTien(danhSachSGK,danhSachSTK);
                    break;
                }
                case 4:{
                    donGiaSTK(danhSachSTK);
                    break;
                }
                case 5:{
                    hienThiSKGCuaNXB(danhSachSGK);
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void menu(){
        System.out.println("1. Nhập danh sách ");
        System.out.println("2. Hiển thị danh sách");
        System.out.println("3. Tổng thành tiền");
        System.out.println("4. Trung bình công đơn giá sách tham khảo");
        System.out.println("5. Hiển thị sách giáo khoa của nhà xuất bản");
    }
    public static void nhapDanhSach(ArrayList<SachGiaoKhoa> danhSachSGK, ArrayList<SachThamKhao> danhSachSTK){
        do {
            System.out.println("1. Sách giáo khoa");
            System.out.println("2. Sách tham khảo");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            switch (chon){
                case 1:{
                    nhapSGK(danhSachSGK);
                    break;
                }
                case 2:{
                    nhapSTK (danhSachSTK);
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void nhapSGK(ArrayList<SachGiaoKhoa> danhSachSGK){
        System.out.println("Nhập sách giáo khoa : ");
        do {
            SachGiaoKhoa sachGiaoKhoa = new SachGiaoKhoa();
            sachGiaoKhoa.nhapThongTin();
            danhSachSGK.add(sachGiaoKhoa);
            System.out.println("1. Nhập tiếp \n\t2. Thoát");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            if (chon == 2)
                return;
        }while (true);
    }
    public static void nhapSTK(ArrayList<SachThamKhao> danhSachSTK){
        System.out.println("Nhập sách tham khảo : ");
        do {
            SachThamKhao sachThamKhao = new SachThamKhao();
            sachThamKhao.nhapThongTin();
            danhSachSTK.add(sachThamKhao);
            System.out.println("1. Nhập tiếp \n\t2. Thoát");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            if (chon == 2)
                return;
        }while (true);
    }
    public static void  hienThiDanhSach(ArrayList<SachGiaoKhoa> danhSachSGK, ArrayList<SachThamKhao> danhSachSTK){
        do {
            System.out.println("1. Sách giáo khoa");
            System.out.println("2. Sách tham khảo");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            switch (chon){
                case 1:{
                    hienThiSGK(danhSachSGK);
                    break;
                }
                case 2:{
                    hienThiSTK (danhSachSTK);
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void hienThiSGK (ArrayList<SachGiaoKhoa> danhSachSGK){
        int stt = 0;
        do {
            System.out.println("STT : " + (stt +1));
            danhSachSGK.get(stt).thongTin();
            System.out.println();
            stt++;
        }while (stt < danhSachSGK.size());
    }
    public static void hienThiSTK (ArrayList<SachThamKhao> danhSachSTK){
        int stt = 0;
        do {
            System.out.println("STT : " + (stt +1));
            danhSachSTK.get(stt).thongTin();
            System.out.println();
            stt++;
        }while (stt < danhSachSTK.size());
    }
    public static void tongThanhTien (ArrayList<SachGiaoKhoa> danhSachSGK, ArrayList<SachThamKhao> danhSachSTK){
        int tienSGK = 0;
        int tienSTK = 0;
        for (int i = 0; i < danhSachSGK.size(); i++) {
            tienSGK += danhSachSGK.get(i).getThanhTien();
        }
        for (int i = 0; i < danhSachSTK.size(); i++) {
            tienSTK += danhSachSTK.get(i).getThanhTien();
        }
        System.out.println("Tổng thành tiền Sách giáo khoa : " +tienSGK);
        System.out.println("Tổng thành tiền sách tham khảo : " + tienSTK);
    }
    public static void donGiaSTK(ArrayList<SachThamKhao> danhSachSTK){
        int sum = 0;
        for (int i = 0; i < danhSachSTK.size(); i++) {
            sum += danhSachSTK.get(i).getDonGia();
        }
        System.out.println("Trung bình công đơn giá sách tham khảo : " + (sum/(danhSachSTK.size() +1)));
    }
    public static void hienThiSKGCuaNXB (ArrayList<SachGiaoKhoa> danhSachSGK){
        System.out.println("Nhà xuất bản : ");
        Scanner scanner = new Scanner(System.in);
        String tenNhaXuatBan = scanner.nextLine();
        for (int i = 0; i < danhSachSGK.size(); i++) {
            if (danhSachSGK.get(i).getNhaXuatBan().equalsIgnoreCase(tenNhaXuatBan)){
                danhSachSGK.get(i).thongTin();
                System.out.println();
            }
        }
    }
}
