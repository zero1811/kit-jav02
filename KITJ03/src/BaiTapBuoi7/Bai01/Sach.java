package BaiTapBuoi7.Bai01;

import java.util.Scanner;

public class Sach {
    private String maSach;
    private String ngayNhap;
    private int donGia;
    private int soLuong;
    private String nhaXuatBan;

    public String getMaSach() {
        return maSach;
    }

    public void setMaSach(String maSach) {
        this.maSach = maSach;
    }

    public String getNgayNhap() {
        return ngayNhap;
    }

    public void setNgayNhap(String ngayNhap) {
        this.ngayNhap = ngayNhap;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public String getNhaXuatBan() {
        return nhaXuatBan;
    }

    public void setNhaXuatBan(String nhaXuatBan) {
        this.nhaXuatBan = nhaXuatBan;
    }
    public void nhapThongTin (){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mã sách : ");
        String maSach = scanner.nextLine();
        this.setMaSach(maSach);
        System.out.println("Ngày nhập : ");
        String ngayNhap = scanner.nextLine();
        this.setNgayNhap(ngayNhap);
        System.out.println("Đơn giá : ");
        int donGia = scanner.nextInt();
        this.setDonGia(donGia);
        System.out.println("Số lượng : ");
        int soLuong = scanner.nextInt();
        this.setSoLuong(soLuong);
        scanner.nextLine();
        System.out.println("Nhà xuất bản : ");
        String nhaXuatBan = scanner.nextLine();
        this.setNhaXuatBan(nhaXuatBan);
    }
    public void thongTin (){
        System.out.println("Mã sách : " + this.getMaSach());
        System.out.println("Ngày nhập : " + this.getNgayNhap());
        System.out.println("Số lượng : " + this.getSoLuong());
        System.out.println("Đơn giá : " + this.getDonGia());
        System.out.println("Nhà xuất bản : " + this.getNhaXuatBan());
    }
}
