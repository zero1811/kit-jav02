package BaiTapBuoi7.Bai02;

import java.util.Scanner;

public class GiaoDichTienTe  extends  GiaoDich{
    private int tyGia;
    private String loaiTien;
    private int thanhTien;
    public int getTyGia() {
        return tyGia;
    }

    public void setTyGia(int tyGia) {
        this.tyGia = tyGia;
    }

    public String getLoaiTien() {
        return loaiTien;
    }

    public void setLoaiTien(String loaiTien) {
        this.loaiTien = loaiTien;
    }

    public int getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(int thanhTien) {
        this.thanhTien = thanhTien;
    }

    public void nhapGiaoDich(){
        super.nhapGiaoDich();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Tỷ giá : ");
        int tyGia = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Loại tiền : ");
        String loaiTien = scanner.nextLine();
        if (loaiTien.equalsIgnoreCase("usd") || loaiTien.equalsIgnoreCase("euro"))
            this.thanhTien = this.getSoLuong() * this.getTyGia() * this.getDonGia();
    }
    public void hienThiGiaoDich(){
        super.hienThiGiaoDich();
        System.out.println("Tỷ giá : " + this.getTyGia());
        System.out.println("Loại tiền : "  + this.getLoaiTien());
        System.out.println("Thành tiền : " + this.getThanhTien());
    }
}
