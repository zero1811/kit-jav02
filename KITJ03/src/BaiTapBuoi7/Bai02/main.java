package BaiTapBuoi7.Bai02;

import java.util.ArrayList;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        ArrayList<GiaoDichVang> danhSachGDV = new ArrayList<>();
        ArrayList<GiaoDichTienTe> danhSachGDT = new ArrayList<>();
        do {
            menu();
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            switch (chon){
                case 1:{
                    nhapGD(danhSachGDV,danhSachGDT);
                    break;
                }
                case 2:{
                    hienThiGD(danhSachGDV,danhSachGDT);
                    break;
                }
                case 3:{
                    tongThanhTien(danhSachGDV,danhSachGDT);
                    return;
                }
                case 4:{
                    trungBinhTienTe(danhSachGDT);
                    break;
                }
                case 5:{
                    giaoDichTren1ty(danhSachGDV,danhSachGDT);
                    break;
                }
                default:return;
            }
        }while (true);

    }
    public static void menu(){
        System.out.println("1. Nhập giao dịch");
        System.out.println("2. Hiển thị giao dịch");
        System.out.println("3.Tổng thành tiền của các giao dịch");
        System.out.println("4. Trung bình thành tiền của giao dịch tiền tệ ");
        System.out.println("5. In các giao dịch có đơn giá trên 1 tỷ");
    }
    public static void nhapGD (ArrayList<GiaoDichVang> danhSachGDV,ArrayList<GiaoDichTienTe> danhSachGDT){
        do {
            System.out.println("1. Nhập giao dịch vàng");
            System.out.println("2. Nhập giao dịch tiền tệ");
            System.out.println("3. Thoát");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            switch (chon){
                case 1:{
                    nhapGDVang(danhSachGDV);
                    break;
                }
                case 2:{
                    nhapGDTien(danhSachGDT);
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void nhapGDVang(ArrayList<GiaoDichVang> danhSachGDV){
        do {
            GiaoDichVang giaoDichVang = new GiaoDichVang();
            System.out.println("Nhập giao dịch : ");
            giaoDichVang.nhapGiaoDich();
            danhSachGDV.add(giaoDichVang);
            System.out.println("1. Nhập tiếp\n2. Thoát");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            if (chon == 2)
                break;
        }while (true);
    }
    public static void nhapGDTien(ArrayList<GiaoDichTienTe> danhSachGDT){
        do {
            GiaoDichTienTe giaoDichTienTe = new GiaoDichTienTe();
            System.out.println("Nhập giao dịch : ");
            giaoDichTienTe.nhapGiaoDich();
            danhSachGDT.add(giaoDichTienTe);
            System.out.println("1. Nhập tiếp\n2. Thoát");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            if (chon == 2)
                break;
        }while (true);
    }
    public static void hienThiGD (ArrayList<GiaoDichVang> danhSachGDV,ArrayList<GiaoDichTienTe> danhSachGDT){
        do {
            System.out.println("1. Hiển thị giao dịch vàng");
            System.out.println("2. Hiển thị giao dịch tiền tệ");
            System.out.println("3. Thoát");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            switch (chon){
                case 1:{
                    hienThiGDV(danhSachGDV);
                    break;
                }
                case 2:{
                    hienThiGDT(danhSachGDT);
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void hienThiGDV(ArrayList<GiaoDichVang> danhSachGDV){
        System.out.println("Giao diện vàng : ");
        for (int i = 0; i < danhSachGDV.size(); i++) {
            danhSachGDV.get(i).hienThiGiaoDich();
            System.out.println();
        }
    }
    public static void hienThiGDT(ArrayList<GiaoDichTienTe> danhSachGDT){
        System.out.println("Giao diện vàng : ");
        for (int i = 0; i < danhSachGDT.size(); i++) {
            danhSachGDT.get(i).hienThiGiaoDich();
            System.out.println();
        }
    }
    public static void tongThanhTien (ArrayList<GiaoDichVang> danhSachGDV,ArrayList<GiaoDichTienTe> danhSachGDT){
        int tongGDVang = 0;
        int tongGDTien = 0;
        for (int i = 0; i < danhSachGDV.size(); i++) {
            tongGDVang += danhSachGDV.get(i).getThanhTien();
        }
        for (int i = 0; i < danhSachGDT.size(); i++) {
            tongGDTien += danhSachGDT.get(i).getThanhTien();
        }
        System.out.println("Tổng thành tiền giao dịch vàng : " + tongGDVang);
        System.out.println("Tổng thành tiền giao dịch tiền tệ : " + tongGDTien);
    }
    public static void trungBinhTienTe(ArrayList<GiaoDichTienTe> danhSachGDT){
        int tongGiaoDich = 0;
        for (int i = 0; i < danhSachGDT.size(); i++) {
            tongGiaoDich += danhSachGDT.get(i).getThanhTien();
        }
        System.out.println("Trung bình thành tiền giao dịch tiền tệ : " + (tongGiaoDich/(danhSachGDT.size()+1)));
    }
    public static void giaoDichTren1ty(ArrayList<GiaoDichVang> danhSachGDV,ArrayList<GiaoDichTienTe> danhSachGDT){
        do {
            System.out.println("1. Giao dịch vàng");
            System.out.println("2. Giao dịch tiền tệ");
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            switch (chon){
                case 1:{
                    System.out.println("Danh sách thành tiền trên 1 tỷ : ");
                    for (int i = 0; i < danhSachGDV.size(); i++) {
                        if (danhSachGDV.get(i).getThanhTien() >= 1000000000){
                            danhSachGDT.get(i).getThanhTien();
                            System.out.println();
                        }
                    }
                    break;
                }
                case 2:{
                    System.out.println("Danh sách thành tiền trên 1 tỷ");
                    for (int i = 0; i < danhSachGDT.size(); i++) {
                        if (danhSachGDT.get(i).getThanhTien() >= 1000000000){
                            danhSachGDT.get(i).getThanhTien();
                            System.out.println();
                        }
                    }
                }
                default:return;
            }
        }while (true);
    }
}
