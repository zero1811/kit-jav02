package BaiTapBuoi7.Bai02;

import java.util.Scanner;

public class GiaoDichVang extends GiaoDich {
    private String loaiVang ;
    private int thanhTien ;

    public String getLoaiVang() {
        return loaiVang;
    }

    public void setLoaiVang(String loaiVang) {
        this.loaiVang = loaiVang;
    }

    public int getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(int thanhTien) {
        this.thanhTien = thanhTien;
    }
    public void nhapGiaoDich (){
        super.nhapGiaoDich();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Loại vàng : ");
        String loaiVang = scanner.nextLine();
        this.thanhTien = this.getDonGia() * this.getSoLuong();
    }
    public void hienThiGiaoDich (){
        super.hienThiGiaoDich();
        System.out.println("Loại vàng : " + this.getLoaiVang());
        System.out.println("Thành tiền : " + this.getThanhTien());
    }
}
