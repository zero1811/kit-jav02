package BaiTapBuoi7.Bai02;

import java.util.Scanner;

public class GiaoDich {
    private String maGiaoDich;
    private String ngayGiaoDich;
    private int donGia;
    private int soLuong;

    public String getMaGiaoDich() {
        return maGiaoDich;
    }

    public void setMaGiaoDich(String maGiaoDich) {
        this.maGiaoDich = maGiaoDich;
    }

    public String getNgayGiaoDich() {
        return ngayGiaoDich;
    }

    public void setNgayGiaoDich(String ngayGiaoDich) {
        this.ngayGiaoDich = ngayGiaoDich;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }
    public void nhapGiaoDich(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mã giao dịch : ");
        String maGiaoDich = scanner.nextLine();
        this.setMaGiaoDich(maGiaoDich);
        System.out.println("Ngày giao dịch : ");
        String ngayGiaoDich = scanner.nextLine();
        this.setNgayGiaoDich(ngayGiaoDich);
        System.out.println("Đơn giá : ");
        int donGia = scanner.nextInt();
        this.setDonGia(donGia);
        System.out.println("Số lượng : ");
        int soLuong = scanner.nextInt();
        this.setSoLuong(soLuong);
    }
    public void hienThiGiaoDich(){
        System.out.println("Mã giao dịch : " + this.getMaGiaoDich());
        System.out.println("Ngày giao dịch : " + this.getNgayGiaoDich());
        System.out.println("Đơn giá : " + this.getDonGia());
        System.out.println("Số lượng : " + this.getSoLuong());
    }
}
