package Buoi10_TruuTuong.Bai3;

public class Nguoi {
    private String hoTen;
    private int tuoi;
    private int namSinh;
    private String ngheNgiep;

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }

    public int getNamSinh() {
        return namSinh;
    }

    public void setNamSinh(int namSinh) {
        this.namSinh = namSinh;
    }

    public String getNgheNgiep() {
        return ngheNgiep;
    }

    public void setNgheNgiep(String ngheNgiep) {
        this.ngheNgiep = ngheNgiep;
    }
}
