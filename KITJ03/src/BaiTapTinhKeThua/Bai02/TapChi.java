package BaiTapTinhKeThua.Bai02;

import java.util.Scanner;

public class TapChi extends TaiLieu {
    private int namXuatBan ;
    private int soTrang;
    private String danhMucXuatBan;

    public TapChi() {
    }

    public TapChi(String maTaiLieu, String tenTaiLieu, String tenNhaXuatBan, int namXuatBan, int soTrang, String danhMucXuatBan) {
        super(maTaiLieu, tenTaiLieu, tenNhaXuatBan);
        this.namXuatBan = namXuatBan;
        this.soTrang = soTrang;
        this.danhMucXuatBan = danhMucXuatBan;
    }

    public int getNamXuatBan() {
        return namXuatBan;
    }

    public void setNamXuatBan(int namXuatBan) {
        this.namXuatBan = namXuatBan;
    }

    public int getSoTrang() {
        return soTrang;
    }

    public void setSoTrang(int soTrang) {
        this.soTrang = soTrang;
    }

    public String getDanhMucXuatBan() {
        return danhMucXuatBan;
    }

    public void setDanhMucXuatBan(String danhMucXuatBan) {
        this.danhMucXuatBan = danhMucXuatBan;
    }
    public void nhapThongTin (){
        super.nhapThongTin();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ngày xuất bản : ");
        int ngayXuatBan = scanner.nextInt();
        this.setNamXuatBan(ngayXuatBan);
        System.out.println("Số trang : ");
        int soTrang = scanner.nextInt();
        this.setSoTrang(soTrang);
        scanner.nextLine();
        System.out.println("Danh mục xuất bản : ");
        String danhMucXuatBan = scanner.nextLine();
        this.setDanhMucXuatBan(danhMucXuatBan);
    }
    public void thongTin () {
        super.thongTin();
        System.out.println("Ngày xuất bản : " + this.getNamXuatBan());
        System.out.println("Số trang : " + this.getSoTrang());
        System.out.println("Danh mục xuất bản : "  + this.getDanhMucXuatBan());
    }
}
