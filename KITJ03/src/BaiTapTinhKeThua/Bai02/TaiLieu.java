package BaiTapTinhKeThua.Bai02;

import java.util.Scanner;

public class TaiLieu {
    private String maTaiLieu;
    private String tenTaiLieu;
    private String tenNhaXuatBan;

    public TaiLieu() {
    }

    public TaiLieu(String maTaiLieu, String tenTaiLieu, String tenNhaXuatBan) {
        this.maTaiLieu = maTaiLieu;
        this.tenTaiLieu = tenTaiLieu;
        this.tenNhaXuatBan = tenNhaXuatBan;
    }

    public String getMaTaiLieu() {
        return maTaiLieu;
    }

    public void setMaTaiLieu(String maTaiLieu) {
        this.maTaiLieu = maTaiLieu;
    }

    public String getTenTaiLieu() {
        return tenTaiLieu;
    }

    public void setTenTaiLieu(String tenTaiLieu) {
        this.tenTaiLieu = tenTaiLieu;
    }

    public String getTenNhaXuatBan() {
        return tenNhaXuatBan;
    }

    public void setTenNhaXuatBan(String tenNhaXuatBan) {
        this.tenNhaXuatBan = tenNhaXuatBan;
    }
    public void nhapThongTin(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mã  : ");
        String maTaiLieu = scanner.nextLine();
        this.setMaTaiLieu(maTaiLieu);
        System.out.println("Tên : ");
        String tenTaiLieu = scanner.nextLine();
        this.setTenTaiLieu(tenTaiLieu);
        System.out.println("Nhà xuất bản : ");
        String nhaXuatBan = scanner.nextLine();
        this.setTenNhaXuatBan(nhaXuatBan);
    }
    public void thongTin(){
        System.out.println("Mã " + this.getMaTaiLieu());
        System.out.println("Tên : " + this.getTenTaiLieu());
        System.out.println("Nhà xuất bản : " + this.getTenNhaXuatBan());
    }
}
