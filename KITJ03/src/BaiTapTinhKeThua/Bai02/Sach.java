package BaiTapTinhKeThua.Bai02;

import java.util.Scanner;

public class Sach  extends  TaiLieu{
    private int soTrang;
    private int namXuatBan;
    private int lanTaiBan;
    private String tenTacGia;

    public Sach() {
    }

    public Sach(String maTaiLieu, String tenTaiLieu, String tenNhaXuatBan, int soTrang, int namXuatBan, int lanTaiBan, String tenTacGia) {
        super(maTaiLieu, tenTaiLieu, tenNhaXuatBan);
        this.soTrang = soTrang;
        this.namXuatBan = namXuatBan;
        this.lanTaiBan = lanTaiBan;
        this.tenTacGia = tenTacGia;
    }

    public int getSoTrang() {
        return soTrang;
    }

    public void setSoTrang(int soTrang) {
        this.soTrang = soTrang;
    }

    public int getNamXuatBan() {
        return namXuatBan;
    }

    public void setNamXuatBan(int namXuatBan) {
        this.namXuatBan = namXuatBan;
    }

    public int getLanTaiBan() {
        return lanTaiBan;
    }

    public void setLanTaiBan(int lanTaiBan) {
        this.lanTaiBan = lanTaiBan;
    }

    public String getTenTacGia() {
        return tenTacGia;
    }

    public void setTenTacGia(String tenTacGia) {
        this.tenTacGia = tenTacGia;
    }
    public void nhapThongTin (){
        super.nhapThongTin();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Số trang : ");
        int soTrang = scanner.nextInt();
        this.setSoTrang(soTrang);
        System.out.println("Năm xuất bản : ");
        int namXuatBan = scanner.nextInt();
        this.setNamXuatBan(namXuatBan);
        System.out.println("Tái bản lần : ");
        int lanTaiBan = scanner.nextInt();
        this.setLanTaiBan(lanTaiBan);
        System.out.println("Tác giả : ");
        scanner.nextLine();
        String tacGia = scanner.nextLine();
        this.setTenTacGia(tacGia);
    }
    public void thongTin(){
        super.thongTin();
        System.out.println("Số trang : " + this.getSoTrang());
        System.out.println("Năm xuất bản : " + this.getNamXuatBan());
        System.out.println("Tái bản lần : " + this.getLanTaiBan());
        System.out.println("Tác giả : " + this.getTenTacGia());
    }
}
