package BaiTapTinhKeThua.Bai02;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Sach sach = new Sach();
        TapChi tapChi = new TapChi();
        System.out.println("Nhập sách : ");
        sach.nhapThongTin();
        System.out.println("Nhập tạp chí : ");
        tapChi.nhapThongTin();
        do {
            menu();
            Scanner scanner = new Scanner(System.in);
            int chon = scanner.nextInt();
            switch (chon){
                case 1:{
                    inThongTin(sach,tapChi);
                    break;
                }
                case 2:{
                    kiemTraNXB(sach,tapChi);
                    break;
                }
                case 3:{
                    kiemTraNamXuatBan(sach,tapChi);
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void menu(){
        System.out.println("1. In thông tin");
        System.out.println("2. Kiểm tra cùng nhà xuất bản");
        System.out.println("3. Kiểm tra có được in cùng một năm hay không");
    }
    public static void inThongTin(Sach sach,TapChi tapChi){
        System.out.println("Thông tin sách : ");
        sach.thongTin();
        System.out.println("Thông tin tạp chí : ");
        tapChi.thongTin();
    }
    public static void kiemTraNXB (Sach sach, TapChi tapChi){
        if (sach.getTenNhaXuatBan().equalsIgnoreCase(tapChi.getTenNhaXuatBan()))
            System.out.println("Cùng nhà xuất bản");
        else
            System.out.println("Không cùng nhà xuất bản");
    }
    public static void kiemTraNamXuatBan(Sach sach, TapChi tapChi){
        if (sach.getNamXuatBan() == tapChi.getNamXuatBan())
            System.out.println("Cùng năm xuất bản");
        else
            System.out.println("Không cùng năm xuất bản");
    }
}
