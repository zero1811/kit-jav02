package BaiTapTinhKeThua.Bai01;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        do {
            menu01();
            int chon01 = scanner.nextInt();
            switch (chon01){
                case 1 :{
                    System.out.println("Nhập thông tin sinh viên : ");
                    SinhVien sv = new SinhVien();
                    sv.nhap();
                    System.out.println("Nhập thông tin giảng viên : ");
                    GiangVien gv = new GiangVien();
                    gv.nhap();
                    menu02();
                    int chon02 = scanner.nextInt();
                    switch (chon02){
                        case 1:{
                            System.out.println("Thông tin sinh viên : ");
                            sv.thongTin();
                            System.out.println("Thông tin giảng viên : ");
                            gv.thongTin();
                            break;
                        }
                        case 2:{
                            if (sv.getNganhHoc().equalsIgnoreCase(gv.getKhoa()))
                                System.out.println("Có cơ hội học");
                            else
                                System.out.println("Không có cơ hội học");
                            break;
                        }
                        default:break;
                    }
                    break;
                }
                case 2:{
                    SinhVien sv01 = new SinhVien();
                    SinhVien sv02 = new SinhVien();
                    GiangVien gv01 = new GiangVien();
                    GiangVien gv02 = new GiangVien();
                    System.out.println("Nhập sinh viên thứ nhất : ");
                    sv01.nhap();
                    System.out.println("Nhập sinh viên thứ hai : ");
                    sv02.nhap();
                    System.out.println("Nhập giảng viên thứ nhất : ");
                    gv01.nhap();
                    System.out.println("Nhập giảng viên thứ hai : ");
                    gv02.nhap();
                    if (sv01.getGioiTinh().equalsIgnoreCase("Nữ"))
                        sv01.thongTin();
                    if (sv02.getGioiTinh().equalsIgnoreCase("Nữ"))
                        sv02.thongTin();
                    if (gv01.getGioiTinh().equalsIgnoreCase("Nữ"))
                        gv01.thongTin();
                    if (gv02.getGioiTinh().equalsIgnoreCase("Nữ"))
                        gv02.thongTin();
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void menu01(){
        System.out.println("1. Nhập thông tin 1 sinh viên và 1 giảng viên");
        System.out.println("2. Nhập thông tin 2 sinh viên và 2 giảng viên");
    }
    public static void menu02(){
        System.out.println("1. In thông tin sinh viên và giảng viên ");
        System.out.println("2. Kiểm tra sinh viên có được học giảng viên đó hay không");
    }
}
