package BaiTapTinhKeThua.Bai01;

import java.util.Scanner;

public class ConNguoi {
    private String hoTen;
    private int namSinh;
    private String queQuan;
    private String gioiTinh;

    public ConNguoi() {
    }

    public ConNguoi(String hoTen, int namSinh, String queQuan, String gioiTinh) {
        this.hoTen = hoTen;
        this.namSinh = namSinh;
        this.queQuan = queQuan;
        this.gioiTinh = gioiTinh;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public int getNamSinh() {
        return namSinh;
    }

    public void setNamSinh(int namSinh) {
        this.namSinh = namSinh;
    }

    public String getQueQuan() {
        return queQuan;
    }

    public void setQueQuan(String queQuan) {
        this.queQuan = queQuan;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }
    public void nhap(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Họ tên : ");
        String hoTen = scanner.nextLine();
        this.setHoTen(hoTen);
        System.out.println("Năm sinh : ");
        int namSinh = scanner.nextInt();
        this.setNamSinh(namSinh);
        scanner.nextLine();
        System.out.println("Quê quán : ");
        String queQuan = scanner.nextLine();
        this.setQueQuan(queQuan);
        System.out.println("Giới tính : ");
        String gioiTinh = scanner.nextLine();
        this.setGioiTinh(gioiTinh);
    }
    public void thongTin(){
        System.out.println("Họ tên : " + this.getHoTen());
        System.out.println("Năm sinh : " + this.getNamSinh());
        System.out.println("Quê quán : " + this.getQueQuan());
        System.out.println("Giới tính : " + this.getGioiTinh());
    }
}
