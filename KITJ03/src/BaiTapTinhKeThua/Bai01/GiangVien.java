package BaiTapTinhKeThua.Bai01;

import java.util.Scanner;

public class GiangVien extends ConNguoi {
    private String khoa;
    private String monHoc;
    private int namKinhNghiem;

    public GiangVien() {
    }

    public GiangVien(String hoTen, int namSinh, String queQuan, String gioiTinh, String khoa, String monHoc, int namKinhNghiem) {
        super(hoTen, namSinh, queQuan, gioiTinh);
        this.khoa = khoa;
        this.monHoc = monHoc;
        this.namKinhNghiem = namKinhNghiem;
    }

    public String getKhoa() {
        return khoa;
    }

    public void setKhoa(String khoa) {
        this.khoa = khoa;
    }

    public String getMonHoc() {
        return monHoc;
    }

    public void setMonHoc(String monHoc) {
        this.monHoc = monHoc;
    }

    public int getNamKinhNghiem() {
        return namKinhNghiem;
    }

    public void setNamKinhNghiem(int namKinhNghiem) {
        this.namKinhNghiem = namKinhNghiem;
    }
    public void nhap(){
        super.nhap();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Khoa :");
        String khoa = scanner.nextLine();
        this.setKhoa(khoa);
        System.out.println("Môn học : ");
        String monHoc = scanner.nextLine();
        this.setMonHoc(monHoc);
        System.out.println("Năm sinh nghiệm : ");
        int namKinhNghiem = scanner.nextInt();
        this.setNamKinhNghiem(namKinhNghiem);
    }
    public void thongTin (){
        super.thongTin();
        System.out.println("Khoa : " + this.getKhoa());
        System.out.println("Môn học : " + this.getMonHoc());
        System.out.println("Năm sinh nghiệm : " + this.getNamKinhNghiem());
    }
}
