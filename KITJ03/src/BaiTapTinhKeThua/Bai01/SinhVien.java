package BaiTapTinhKeThua.Bai01;

import java.util.Scanner;

public class SinhVien extends ConNguoi {
    private String maLop;
    private String nganhHoc;
    private int khoa;

    public SinhVien() {
    }

    public SinhVien(String hoTen, int namSinh, String queQuan, String gioiTinh, String maLop, String nganhHoc, int khoa) {
        super(hoTen, namSinh, queQuan, gioiTinh);
        this.maLop = maLop;
        this.nganhHoc = nganhHoc;
        this.khoa = khoa;
    }

    public String getMaLop() {
        return maLop;
    }

    public void setMaLop(String maLop) {
        this.maLop = maLop;
    }

    public String getNganhHoc() {
        return nganhHoc;
    }

    public void setNganhHoc(String nganhHoc) {
        this.nganhHoc = nganhHoc;
    }

    public int getKhoa() {
        return khoa;
    }

    public void setKhoa(int khoa) {
        this.khoa = khoa;
    }
    public void  nhap(){
        super.nhap();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mã lớp : ");
        String maLop = scanner.nextLine();
        this.setMaLop(maLop);
        System.out.println("Ngành học : ");
        String nganhHoc = scanner.nextLine();
        this.setNganhHoc(nganhHoc);
        System.out.println("Khóa : ");
        int khoa = scanner.nextInt();
        this.setKhoa(khoa);
    }
    public void thongTin (){
        super.thongTin();
        System.out.println("Mã lớp : " + this.getMaLop());
        System.out.println("Ngành học : "  + this.getNganhHoc());
        System.out.println("Khóa : " + this.getKhoa());
    }
}
